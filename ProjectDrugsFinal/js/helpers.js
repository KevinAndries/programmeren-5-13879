/**
 * Geolocatie van de telefoon ophalen
 *
 */
/**
 * Een html element creëren van het type opgegeven in de tag parameter.
 * Plaats de tekst opgegeven in de text parameter in het gemaakte element.
 *
 * @param {string} text Text to be placed in the html element.
 * @param {string} tag type van het te maken html element.
 */
var makeTextElement = function(text, tag) {
    if (!tag) {
        tag = 'P';
    }
    var elem = document.createElement(tag);
    var textNode = document.createTextNode(text);
    elem.appendChild(textNode);
    return elem;
}

/**
 * Een button html element maken met een specifieke tekst erin en een bepaald icoon.
 *
 * @param {string} text Text to be placed in the button element.
 * @param {string} icon klassenaam van het te tonen icoon.
 */
var makeButton = function(text, icon) {
    var buttonElem = document.createElement('BUTTON');
    buttonElem.setAttribute('type', 'submit');
    buttonElem.setAttribute('class', 'tile');
    var iconElem = document.createElement('SPAN');
    iconElem.setAttribute('class', icon);
    buttonElem.appendChild(iconElem);
    var screenReaderTextElem = document.createElement('SPAN');
    screenReaderTextElem.setAttribute('class', 'screen-reader-text');
    var textElem = document.createTextNode(text);
    screenReaderTextElem.appendChild(textElem);
    buttonElem.appendChild(screenReaderTextElem);
    return buttonElem;
}

/**
 * Een commandPanel element maken.
 *
 */
var makeCommandPanel = function() {
    var elem = document.createElement('DIV');
    elem.setAttribute('class', 'command-panel');
    return elem;
}
