var vos = {
    'onload': function() {
        // alleen tijdens het ontwikkelen
        vos.clearLocalStorage();
        vos.getPosition();
        document.body.addEventListener('click', dispatcher, false);
        /**
         * Vul het model in vanaf de JSON bestanden.
         */
        $http('data/identity.json')
            .get()
            .then(function(data) {
                if (!localStorage.getItem('identity')) {
                    localStorage.setItem('identity', data);
                }
                model.identity = JSON.parse(localStorage.getItem('identity'));
                var payload = {};
                return $http('data/procedureList.json').get(payload);
            })
            .then(function(data) {
                if (!localStorage.getItem('procedureList')) {
                    localStorage.setItem('procedureList', data);
                }
                model.procedureList = JSON.parse(localStorage.getItem('procedureList'));
                var payload = {
                    'id': 1
                };
                return $http('data/position.json').get(payload);
            })
            .then(function(data) {
                if (!localStorage.getItem('position')) {
                    localStorage.setItem('position', data);
                }
                model.position = JSON.parse(localStorage.getItem('position'));
                var payload = {};
                return $http('data/organisationList.json').get(payload);
            })
            .then(function(data) {
                if (!localStorage.getItem('organisationList')) {
                    localStorage.setItem('organisationList', data);
                }
                model.organisationList = JSON.parse(localStorage.getItem('organisationList'));
                model.loaded = true;
            })
            .catch(function(data) {
                model.loaded = false;
            });
    },
    /**
     * Geolocatie van de telefoon ophalen
     *
     */
    getPosition: function() {
        var options = {
            maximumAge: 3600000,
            timeout: 6000,
            enableHighAccuracy: false
        }
        var onSuccess = function(pos) {
            model.position.latitude = pos.coords.latitude.toFixed(4);
            model.position.longitude = pos.coords.longitude.toFixed(4);
            vos.setMyLocation();
            render.identity('#identity');
            view['home']['index']();
        };
        var onError = function(error) {
            // stel in op hoofdzetel
            model.position.latitude = 51.1771;
            model.position.longitude = 4.3533;
            vos.setMyLocation();
            render.identity('#identity');
            view['home']['index']();
        };
        var watchID = navigator.geolocation.getCurrentPosition(onSuccess, onError, options);
    },
    /**
     * De dichtsbijzijnde organisatie ophalen.
     *
     */
    setMyLocation: function() {
        var i;
        for (i = 0; i < model.organisationList.length; i++) {
            if (model.organisationList[i].latitude >= model.position.latitude) {
                model.myLocation = model.organisationList[i];
                done = true;
                return;
            }
        }
        if (i = model.organisationList.length) {
            model.myLocation = model.organisationList[i - 1];
        }
    },
    /**
     * nagaan of de gebruikersnaam en het paswoord in de personList zitten
     * dit is geen veilige manier om aan te melden en moet je niet in productie app gebruiken
     */
    login: function() {
        var userName = document.getElementById('userName').value;
        var password = document.getElementById('password').value;

        if (userName) {
            $http('data/personList.json')
                .get()
                .then(function(responseText) {
                    var person = JSON.parse(responseText);
                    var userIdentity = person.list.find(function(item) {
                        return item.userName === userName && item.password === password
                    });
                    if (userIdentity) {
                        localStorage.removeItem('identity');
                        localStorage.setItem('identity', JSON.stringify(userIdentity));
                        // identity = JSON.parse(localStorage.getItem('identity'));
                        model.identity = userIdentity;
                        // update location and user info
                        vos.getPosition();
                    }
                    else {
                        alert('ongeldige gebruikersnaam of paswoord');
                    }
                });
        }
        else {
            alert('Je moet een gebruikersnaam opgeven!');
        }
    },
    /**
     * Ga naar de floor met de opgegeven view id en toon de tekst
     * in title in het h1 element.
     *
     * @param {string} view text id van de floor die getoond moet worden.
     * @param {string} title tekst die in het h1 element geplaatst moet worden.
     */
    navigateTo: function(view, title) {
        window.location.href = '#' + view;
        var h1 = document.querySelector('#' + view + ' h1');
        if (title && h1) {
            h1.innerHTML = title;
        }
    },
    /**
     * Tijdens het ontwikkelen van de app kan het nodig
     * zijn de localStorage leeg te maken
     */
    clearLocalStorage: function() {
        localStorage.removeItem('identity');
        localStorage.removeItem('procedureList');
        localStorage.removeItem('position');
        localStorage.removeItem('organisationList');
    },
    /**
     * Voorbereiding versturen sms.
     *
     * @param {string} number telefoonnummer waarnaar sms gestuurd moet worden.
     * @param {string} message boodschap die verstuurd zal worden.
     */
    smsPrepare: function(number, messageText) {
        // number = '0486788723';
        var message = messageText + '\n' +
            model.identity.firstName + ' ' + model.identity.lastName + '\n' +
            model.myLocation.name + '\n' +
            model.myLocation.street + '\n' +
            model.myLocation.postalCode + ' ' + model.myLocation.city + '\n' +
            number;
        smsSend(number, message);
    }
};

var model = {
    loaded: false,
    identity: {},
    procedureList: {},
    organisationList: {},
    position: {},
    myLocation: {}
}

var view = {
    'home': {
        'index': function() {
            window.location.href = "#home-index";
        },
        'loggingIn': function() {
            window.location.href = "#home-loggingIn";
        }
    },
    'drugs': {
        'index': function() {
            window.location.href = "#drugs-index";
        }
    },
    'procedure': function(title) {
        vos.navigateTo('view-procedure', title);
    }
};



var render = {
    'identity': function(querySelector) {
        var elem = document.querySelector(querySelector);
        elem.innerHTML = '';
        elem.appendChild(makeTextElement(model.identity.firstName + ' ' + model.identity.lastName, 'h2'))
        elem.appendChild(makeTextElement(model.identity.function, 'h3'));
        elem.appendChild(makeTextElement(model.identity.mobile, 'h4'));
        elem.appendChild(makeTextElement(model.myLocation.name));
        elem.appendChild(makeTextElement(model.myLocation.street));
        elem.appendChild(makeTextElement(model.myLocation.phone));
        return elem;
    },
    'procedure': {
        'make': function(procedureCode) {
            var userRole = model.identity.role.toUpperCase();
            var procedure = model.procedureList.list.find(function(item) {
                return item.code === procedureCode;
            });
            var role = procedure.role.find(function(item) {
                return item.code === userRole;
            });
            elem = render.identity('#view-procedure .show-room');
            role.step.forEach(function(item, index) {
                var step = document.createElement('DIV');
                step.setAttribute('class', 'step');
                step.appendChild(makeTextElement(item.title, 'h2'));
                var commandPanelElem = makeCommandPanel();
                item.action.forEach(function(item) {
                    commandPanelElem.appendChild(render.procedure[item.code](item));
                });
                if (commandPanelElem.innerHTML !== '') {
                    step.appendChild(commandPanelElem);
                }
                elem.appendChild(step);
            });
        },
        'TEL': function(item) {
            if (model.identity.loggedIn) {
                var buttonElement = makeButton('Tel', 'icon-phone');
                // buttonElement.addEventListener('click', telDrugsDetection(item.phoneNumber));
                return buttonElement;
            }
            else {
                return makeTextElement(item.code + ' ' + item.phoneNumber, 'P');
            }
        },
        'SMS': function(item) {
            if (model.identity.loggedIn) {
                var buttonElement = makeButton('Tel', 'icon-send');
                buttonElement.addEventListener('click', function() {
                    vos.smsPrepare(item.phoneNumber, 'Brandmelding');
                });
                return buttonElement;
            }
            else {
                return makeTextElement(item.code + ' ' + item.phoneNumber, 'P');
            }
        },
        'LIST': function(item) {
            var listElement = document.createElement('OL');
            listElement.setAttribute('class', 'index');
            item.list.forEach(function(item) {
                listElement.appendChild(makeTextElement(item.title, 'li'))
            });
            return listElement;
        }
    }
};

//Via de controller komen we in het beginmenu
var controller = {
    'home': {
        'index': function() {
            vos.getPosition();
            vos.setMyLocation();
            render.identity('#identity');
            view['home']['index']();
        },
        'loggingIn': view['home']['loggingIn'],
        'login': vos.login
    },
    'drugs': {
        'index': view['drugs']['index'],
        'detection': function() {
            render.procedure.make('MD');
            view['procedure']('melding dealen');
        },
        'symptoms': function() {
            render.procedure.make('SYM');
            view['procedure']('symptonen');
        },
        'EHBO': function() {
            render.procedure.make('EHBO');
            view['procedure']('EHBO');
        },
    },
    'page': {
        'previous': function() {
            window.history.back();
        }
    }
};

/**
 * Dispatch methode die de use case uitvoert die overeenkomt
 * met een de gevraagde interactie van de gebruiker.
 *
 * @param {object} e verwijzing naar het dom element dat het event heeft afgevuurd.
 */
var dispatcher = function(e) {
    var target = e.target;
    if (target.tagName === 'SPAN') {
        target = target.parentNode;
    }
    if (target.getAttribute('name') === 'uc') {
        var uc = target.getAttribute('value');
        var path = uc.split('/');
        var entity = path[0] === undefined ? 'none' : path[0];
        var action = path[1] === undefined ? 'none' : path[1];
        var view = entity + '-' + action;
        // alert (entity + '/' + action);
        if (controller[entity][action]) {
            controller[entity][action]();
        }
        else {
            alert('ongeldige url ' + uc);
        }
    }
};

vos.onload();