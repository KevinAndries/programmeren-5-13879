function onAppReady() {
    if( navigator.splashscreen && navigator.splashscreen.hide ) {   // Cordova API detected
        navigator.splashscreen.hide() ;
    }
}
document.addEventListener("app.Ready", onAppReady, false) ;

function getPosition() {
     navigator.geolocation.getCurrentPosition(onSuccess, onError);   
}

function onSuccess(position) {
    var element = document.getElementById('geolocation');
    element.innerHTML = 'Latitude: ' + position.coords.latitude + '<br />' +
        'Longitude: ' + position.coords.longitude + '<br />' +
        'Altitude: ' + position.coords.altitude + '<br />' +
        'Accuracy: ' + position.coords.accuracy + '<br />' +
        'Altitude Accuracy: ' + position.coords.altitudeAccuracy + '<br />' +
        'Heading: ' + position.coords.heading + '<br />' +
        'Speed: ' + position.coords.speed + '<br />' +
        'Timestamp: ' + position.timestamp + '<br />';
}

// onError Callback receives a PositionError object
//
function onError(error) {
    alert('code: ' + error.code + '\n' +
        'message: ' + error.message + '\n');
}

function addPre(data) {
    var pre = document.createElement('pre');
    var t = document.createTextNode(data);
    pre.appendChild(t);
    document.body.appendChild(pre);
}

/**
 *  Vul het model in vanaf de json bestanden
 * 
 */
function loadData() {
    $http('data/procedureList.json')
        .get()
        .then(function (response) {
            //addPre(response);
            localStorage.setItem('procedureList', response);
            return $http('data/organisationList.json').get();
        })
        .then(function (organisation) {
            //addPre(organisation);
            localStorage.setItem('organisationList', organisation);
            return $http('data/identity.json').get();
        })
        .then(function (response) {
            //addPre(response);
            localStorage.setItem('identity', response);
            return $http('data/position.json').get();
        })
        .then(function (response) {
           localStorage.setItem('position', response);
        })
        .catch(function (response) {
            addPre('Error: ' + response);
        });
}

// /**
//  * Dispath methode die use cases uitvoert
//  * @param (object) e verwijzing naar het dom element
//  * dat het event heeft afgevuurd (dit element wordt de target genoemd)
//  */
// var appDispatcher = function(e) {
//     var target = e.target;
//     if (target.tagName == "SPAN") {
//         target = target.parentNode;
//     }
//     if (target.getAttribute('name') == 'uc') {
//         var uc = target.getAttribute('value');
//         var path = uc.split('/');
//         var entity = path[0] == undefined ? 'none' : path[0];
//         var action = path[1] == undefined ? 'none' : path[1];
//         switch (entity) {
//             case 'home' :
//                 switch (action) {
//                     case 'index' :
//                         window.location.href="#home-index";
//                         break;
//                 }
//                 break;
//             case 'drugs' :
//                 switch (action) {
//                     case 'index' :
//                         window.location.href="#drugs-index";
//                         break;
//                 }
//                 break;
//         }
//     }
// }