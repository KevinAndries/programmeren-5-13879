var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');
        vos.getPosition();
        console.log('Received Event: ' + id);
    }
};

app.initialize();

/**
 * De Cordova plugin gebruiken om een sms te versturen
 *
 * @param {string} number telefoonnummer waarnaar sms gestuurd moet worden.
 * @param {string} message de tekst van de te versturen sms.
 */
var smsSend = function (number, message) {
    // CONFIGURATION
    var options = {
        replaceLineBreaks: false, // true to replace \n by a new line, false by default
        android: {
            // intent: 'INTENT'  // send SMS with the native android SMS messaging
            intent: '' // send SMS without open any other app
        }
    };

    var success = function () {
        alert('SMS is verstuurd!');
    };

    var error = function (e) {
        alert('SMS is niet verstuurd:' + e);
    };
    if (typeof sms === 'undefined' || typeof sms.send === 'undefined') {
        alert('SMS send is undefined. Would have sent error');
    } else {
        sms.send(number, message, options, success, error);
    }
}