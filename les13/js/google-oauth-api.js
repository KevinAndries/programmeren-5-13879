function loadAuthClient() {
    // Load the API cliebt abd auth library
    // alert('De Google API Client bibliotheek is geladen.');
    gapi.load('client:auth2', initAuth);
}

function initAuth() {
    gapi.auth2.init({
        client_id: clientId,
        scope: scopes.join(' '), 
        immediate: true
    }).then(function() {
        // listen to the sign-in state changes
        updateSignInStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
        gapi.auth2.getAuthInstance().isSignedIn.listen(updateSignInStatus);
        signInButton.addEventListener('click', signIn);
        signOutButton.addEventListener('click', signOut);
    }).then(handleInitialSignInStatus);
}

function updateSignInStatus(isSignedIn) {
    if (isSignedIn) {
        signInButton.style.display = 'none';
        signOutButton.style.display = 'block';
    } else {
        signInButton.style.display = 'block';
        signOutButton.style.display = 'none';
        
    }
}

function signIn(event) {
    gapi.auth2.getAuthInstance().signIn();
}

function signOut(event) {
    gapi.auth2.getAuthInstance().signOut();
}
