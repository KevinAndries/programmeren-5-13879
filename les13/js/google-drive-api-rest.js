var getFiles = function(callbackFunction) {
    var ajax = new Ajax();
    var accessToken = 
        
        gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    // Google CORS
    var url = "https://www.googleapis.com/drive/v3/files";
    url += '?q=not+trashed';
    url += '&fields=files(iconLink%2Cname%2Cid%2CmimeType)';
    ajax.getRequest(url, callbackFunction, 'text', accessToken);
}

/**
 * We weten niet van te voren wat we met de bestanden gaan doen,
 * vandaar dat je de callback methode als parameter kan meegeven.
 * Wordt er geen parameters meegegeven, gaan we ervan uit dat je
 * gewoon een lijst van de opgehaalde bestanden wilt tonen.
 * This is a simple listing with filters to the parent directory
 * and not trashed (m.a.w. gedeleted door de gebruiker).
 *
 * @param {id} de id van de te tonen folder
 * @param {name} de naam van de te tonen folder
 * @param {callbackFunction} the callback function for the ajax call
 */
function getFilesInFolder(id, name, callback = showFiles) {
    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    // or this: gapi.auth.getToken().access_token;
    var ajax = new Ajax();
    // google CORS
    var url = 'https://www.googleapis.com/drive/v3/files';
    // query
    url += '?q=';
    url += 'not+trashed';
    url += '+and+';
    url += '\'' + id + '\'+in+parents';
    // fields
    url += '&';
    url += 'fields=files(iconLink%2Cname%2CmimeType%2Cid%2Cparents)';
    //alert(url);
    ajax.getRequest(url, showFiles, 'text', accessToken);
    // onthoud de id en de naam van de geselecteerde folder
    document.getElementById('currentFolderId').value = id;
    document.getElementById('currentFolderName').value = name;
}

var showFiles = function(responseText) {
    var explorer = document.querySelector("#explorer");
    var table = document.createElement('TABLE');
    // maak array van json antwoord van de server
    var response = JSON.parse(responseText);
    response.files.map(function (elem) {
        var row = document.createElement('TR');
        var iconCol = document.createElement('TD');
        if (elem.mimeType == 'application/vnd.google-apps.folder') {
            var folderButton = document.createElement('INPUT');
            folderButton.setAttribute('type', 'image');
            folderButton.setAttribute('src', elem.iconLink);
            iconCol.appendChild(folderButton);
        } else {
            var icon = document.createElement('IMG');
            icon.setAttribute('src', elem.iconLink);
            icon.setAttribute('alt', 'Icoon bestand');
            iconCol.appendChild(icon);
        }
        var nameCol = document.createElement('TD');
        var nameText = document.createTextNode(elem.name);
        nameCol.appendChild(nameText);
        row.appendChild(iconCol);
        row.appendChild(nameCol);
        table.appendChild(row);
        explorer.appendChild(table);
    });
    showFeedback(responseText);
}