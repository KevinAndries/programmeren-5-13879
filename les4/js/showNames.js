
// Optie 1

function toonKevin() {
    var persoon = [];
    persoon['voornaam'] = 'Kevin';
    persoon['achternaam'] = 'Andries';
    persoon['straat'] = 'Camille Huysmanslaan 37/15';
    persoon['stad'] = 'Antwerpen';
    persoon['postcode'] = '2020';
    persoon['geslacht'] = 'Man';
    persoon['naam'] = function() {return this.voornaam + ' ' + this.achternaam};
    
    document.querySelector('legend').innerText = persoon.naam();
    document.getElementById('voornaam').value = persoon.voornaam;
    document.getElementById('achternaam').value = persoon['achternaam'];
    document.getElementById('straat').value = persoon.straat;
    document.getElementById('stad').value = persoon.stad;
    document.getElementById('postcode').value = persoon.postcode;
    document.getElementById('geslacht').value = persoon.geslacht;

}


function toonCaroline() {
    
    var persoon = {
    voornaam : 'Caroline',
    achternaam:  'De Weerdt',
    straat : 'Camille Huysmanslaan 37/15',
    stad : 'Antwerpen',
    postcode : '2020',
    geslacht : 'Vrouw',
    naam: function() { return this.voornaam + ' ' + this.achternaam;}
    
    }
    document.querySelector('legend').innerText = persoon.naam();
    document.getElementById('voornaam').value = persoon.voornaam
    document.getElementById('achternaam').value = persoon['achternaam'];
    document.getElementById('straat').value = persoon.straat
    document.getElementById('stad').value = persoon.stad;
    document.getElementById('postcode').value = persoon.postcode;
    document.getElementById('geslacht').value = persoon.geslacht;
}




// Optie 2 met for lus

function toonKevinArray() {
  // Indit voorbeeld gebruiken we een array. We kunnen
  // integer index gebruiken om de array te doorlopen
  
  // een array met de id's van de input velden
  var field = [];
  field[0] = 'voornaam';
  field[1] = 'achternaam';
  field[2] = 'straat';
  field[3] = 'postcode';
  field[4] = 'stad';
  field[5] = 'geslacht';


  var persoon = [];
  persoon[0] = 'Kevin';
  persoon[1] = 'Andries';
  persoon[2] = 'Camille Huysmanslaan 37/15';
  persoon[3] = '2020';
  persoon[4] = 'Antwerpen';
  persoon[5] = 'Man';
  persoon[6] = function() {
    return this[0] + ' ' + this[1];
  }
  document.querySelector('legend').innerText = persoon[6]();
  for (i = 0; i < persoon.length; i++) {
    document.getElementById(field[i]).value = persoon[i];
  }
}

function toonKevin2() {

  var persoon = [];
  persoon['voornaam'] = 'Kevin';
  persoon['achternaam'] = 'Andries';
  persoon['straat'] = 'Camille Huysmanslaan 37/15';
  persoon['stad'] = 'Antwerpen';
  persoon['postcode'] = '2020';
  persoon['geslacht'] = 'Man';
  persoon['naam'] = function() {
    return this.voornaam + ' ' + this.familienaam;
  }

// function toonCaroline2() {

//   var persoon = [];
//   persoon['voornaam'] = 'Caroline';
//   persoon['achternaam'] = 'De Weerdt';
//   persoon['straat'] = 'Camille Huysmanslaan 37/15';
//   persoon['stad'] = 'Antwerpen';
//   persoon['postcode'] = '2020';
//   persoon['geslacht'] = 'Vrouw';
//   persoon['naam'] = function() {
//     return this.voornaam + ' ' + this.familienaam;
//   }

  document.querySelector('legend').innerText = persoon.naam();
  for (eigenschap in persoon) {
      document.getElementById(eigenschap).value = persoon[eigenschap];
  }
}




// Optie 3 Object Constructor

function Persoon(voornaam, familienaam, straat, postcode, stad, geslacht) {
   this.voornaam = voornaam;
   this.familienaam = familienaam;
   this.straat = straat;
   this.postcode = postcode;
   this.stad = stad;
   this.geslacht = geslacht;
   this.naam = function() {
     return this.voornaam + ' ' + this.familienaam;
   }
 }

 function toonKevin3() {
   var kevin = new Persoon('Kevin', 'Andries',
     'Camille Huysmanslaan 37/15', '2020', 'Antwerpen', 'Man');
   document.querySelector('legend').innerText = kevin.naam();
   toon(kevin);
 }

 function toonCaroline3() {

   var caroline = new Persoon('Caroline', 'De Weerdt',
     'Camille Huysmanslaan 37/15', '2020', 'Antwerpen', 'Vrouw');
   toon(caroline);
 }

 function toon(persoon) {
   document.querySelector('legend').innerText = persoon.naam();
   var eigenschap;
   for (eigenschap in persoon) {
     // alleen die iegenschappen waarvoor er een
     // input veld is voorzien
     if (document.getElementById(eigenschap).tagName == 'INPUT') {
       document.getElementById(eigenschap).value = persoon[eigenschap];
     }
   }
 }