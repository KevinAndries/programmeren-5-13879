var loadData = function() {
    var callback = {
        procedureListLoaded: function(data) {
            var pre = document.createElement('PRE');
            var t = document.createTextNode(data);
            pre.appendChild(t);
            document.body.appendChild(pre);
        },
        organisationListLoaded: function(data) {
            var pre = document.createElement('PRE');
            var t = document.createTextNode(data);
            pre.appendChild(t);
            document.body.appendChild(pre);
        },
        positionLoaded: function(data) {
            var pre = document.createElement('PRE');
            var t = document.createTextNode(data);
            pre.appendChild(t);
            document.body.appendChild(pre);
        },
        error: function(data) {
            var p = document.createElement('P');
            p.innerHTML = '2 error: ' + data;
            p.style.color = '#ff0000';
            document.body.appendChild(p);
        }
    };

    $http('data/procedureList.json')
        .get()
        .then(function(data) {
            callback.procedureListLoaded(data);
            var payload = {};
            return $http('data/plopperdeplop.json').get(payload);
        })
       .then(function(data) {
            callback.organisationListLoaded(data);
            var payload = {};
            return $http('data/position.json').get(payload);
        })
        .then(function(data) {
            callback.positionLoaded(data);
            var payload = {};
            return $http('data/identity.json').get(payload);
        })
        .then(function(data) {
            callback.positionLoaded(data);
            var payload = {};
            return $http('data/identity.json').get(payload);
        })
        .catch(callback.error);
}
