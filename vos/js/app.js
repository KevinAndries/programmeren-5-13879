function getPosition() {
     navigator.geolocation.getCurrentPosition(onSuccess, onError);   
}

function onSuccess(position) {
    var element = document.getElementById('geolocation');
    element.innerHTML = 'Latitude: ' + position.coords.latitude + '<br />' +
        'Longitude: ' + position.coords.longitude + '<br />' +
        'Altitude: ' + position.coords.altitude + '<br />' +
        'Accuracy: ' + position.coords.accuracy + '<br />' +
        'Altitude Accuracy: ' + position.coords.altitudeAccuracy + '<br />' +
        'Heading: ' + position.coords.heading + '<br />' +
        'Speed: ' + position.coords.speed + '<br />' +
        'Timestamp: ' + position.timestamp + '<br />';
}

// onError Callback receives a PositionError object
//
function onError(error) {
    alert('code: ' + error.code + '\n' +
        'message: ' + error.message + '\n');
}

function addPre(data) {
    var pre = document.createElement('pre');
    var t = document.createTextNode(data);
    pre.appendChild(t);
    document.body.appendChild(pre);
}

/**
 *  Vul het model in vanaf de json bestanden
 * 
 */
function loadData() {
    $http('data/procedureList.json')
        .get()
        .then(function (response) {
            addPre(response);
            return $http('data/organisationList.json').get();
        })
        .then(function (plopperdeplop) {
            addPre(plopperdeplop);
            return $http('data/identitys.json').get();
        })
        .then(function (response) {
            addPre(response);
            return $http('data/position.json').get();
        })
        .catch(function (response) {
            addPre('Error: ' + response);
        });
}