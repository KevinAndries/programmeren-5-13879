// De Ajax klasseconstructor:
// hier voegen we de eigenschappen en methoden toe die we nodig hebben om met het XMLHttpRequest object te kunnen werken.
// Definiëren van de properties
// this verwijst hier niet naar het instantie van het object (C#), maar de referentie in het geheugen waarin de functie wordt gedeclareerd
function Ajax() {
    this.request = null;
    this.url = null;
    this.status = null;
    this.statusText = '';
    this.method = 'GET';
    this.asynchronousFlag = true;
    this.postData = null;
    this.readyState = null;
    this.responseText = null;
    this.responseXML = null;
    this.responseHandle = null;
    this.errorHandle = null;
    this.responseFormat = 'text', 'xml', 'object'; // 'text', 'xml', 'object'
    this.mimeType = null;
    this.accessToken = null; // te gebruiken met oAuth
    this.contentType = 'application/x-www-form-urlencoded'; // default content-type

    // Methoden:
    // Een XMLHttpRequest object maken
    // Dit wordt gebruikt voor het updaten van de pagina zonder de gehele pagina opnieuw te moeten laden
    // Het XMLHttpRequest object zal op de achtergrond data uitwisselen met de server.
    this.initialize = function() {
        if (!this.request) {
            try {
                // Probeer een object te creëren voor Firefox, Safari, IE7
                this.request = new XMLHttpRequest();
            }
            catch (e) {
                try {
                    // Probeer een object te creëren voor latere versies IE
                    this.request = new ActiveXObject('MSXML2.XMLHTTP');
                }
                catch (e) {
                    try {
                        // Probeer een object te maken met oudere versies van IE
                        this.request = new ActiveXObject('Microsoft.XMLHTTP');
                    }
                    catch (e) {
                        // Kan geen XMLHttpRequest maken
                        return false;
                    }
                }
            }
        }
        return true;
    };

    // Een request instellen
    this.setupRequest = function() {
        // Probeer een XMLHttpRequest object aan te maken, als het niet lukt --> retourneren
        if (!this.initialize()) {
            alert('Het XMLHttpRequest object kan niet gecreëerd worden!');
            return;
        }
        // Een XMLHttpRequest object is met succes gecreëerd
        // method: GET of POST: Type van de aanvraag
        // URL: Locatie van het bestand op de server
        // async: Asynchroon uitboeren van de aanvraag
        this.request.open(this.method, this.url, this.asynchronousFlag);
        // Het aanmaken van een geauthenticeerd request door gebruik te maken van een request header via een accessToken.
        if (this.accessToken) {
            this.request.setRequestHeader('Authorization', 'Bearer ' + this.accessToken);
        }


        //Content-type beschrijft de gegevens die in de body worden meegegeven. 
        //Hierdoor kan de ontvanger de juiste methode kiezen om aan de gebruiker te presenteren/gegevens verwerken.
        if (this.method == 'POST') {
            this.request.setRequestHeader('Content-Type', this.contentType);
        }



        //Hieronder gaan we aangeven wat er moet gebeuren wanneer er een response ontvangen wordt.
        //onreadystatechange eventhandler moeten we dus instellen voor de request verzonden wordt.
        //dit doen we adhv een onreadystatechange event van het XMLHttpRequest object.
        var me = this; // fix loss of scope in inner function
        // opslaan van een functie/naam functie die automatisch wordt uigevoerd wanneer de readyState eigenschap wijzigt. 
        this.request.onreadystatechange = function() {
            // handel de event af als de request volledig uitgevoerd is
            // bevat de status van het XMLHttpRequest. De status gaat van 0 naar 4:
            // 0: request is niet geïnitialiseerd
            // 1: connectie met de server is gemaakt
            // 2: request ontvangen
            // 3: request is in behandeling
            // 4: request is afgewerkt en het antwoord staat klaar
            var response = null;
            //Het XMLHttpRequest vuurt een event af telkens wanneer readyState wijzigt 
            if (me.request.readyState == 4) {
                // Bereid het antwoord voor in het gewenste formaat
                // callback definiëren om het antwoord van de server te verwerken
                switch (me.responseFormat) {
                    case 'text':
                        // retourneren als string
                        response = me.request.responseText;
                        break;
                    case 'xml':
                        // retrourneren als XML data
                        response = me.request.responseXML;
                        break;
                    case 'object':
                        response = request;
                        break;
                }
                // antwoord van de server verwerken
                // afhandelen van fouten door antwoord te geven via de status codes
                // Indien ok, antwoord dorgeven aan functie zodat het antwoord afgehandeld kan worden. 
                if (me.request.status >= 200 && me.request.status <= 299) {
                    if (!me.responseHandle) {
                        alert('Geen response handler gedefiniëerd voor dit ' +
                            'XMLHttpRequest object.');
                        return;
                    }
                    else {
                        me.responseHandle(response);
                    }
                }
                else {
                    me.errorHandle(response);
                }
            }
        };
        // Verstuur de request met de send method van het XMLHttpRequest object
        this.request.send(this.postData);
    };


















    //GET Request versturen
    //4 zaken nodig voor Ajax klasse om te kunnen werken:
    //een URL
    //een handler functie voor het antwoord
    //derde optionele parameter waarmee je het standaard ingestelde formaat kan wijzigen
    //met de vierde parameter kan je een access token meegeven om een geverifiëerde request te maken
    this.getRequest = function(url, handle, format, accessToken) {
        this.url = url;
        this.responseHandle = handle;
        this.responseFormat = format || 'text';
        this.accessToken = accessToken;
        this.setupRequest();
    };
    
    
    
    
    
    
    
    

    //POST Request versturen
    //Gegevens in de body van de request kunnen naar de server verzonden worden. Dit aan de hand van twee extra parameteres
    //format: het formaat waarin de gegevens door de server moeten worden teruggestuurd
    //contentType: Type van gegevens die verzonden worden zodat server weet welk type te verwerken
    //voor Google API call's is dat bijvoorbeeld application/json; zo weet de server van welk soort gegevenstype de gegevens zijn die binnenkomen
    this.postRequest = function(url, postData, handle, format, contentType, accessToken) {
        this.url = url;
        this.responseHandle = handle;
        this.responseFormat = format || 'text';
        this.contentType = contentType || 'application/x-www-form-urlencoded'
        this.accessToken = accessToken;
        this.method = 'POST';
        this.postData = postData;
        this.setupRequest();
    };
    
    
    
    
    
    
    //DELETE Request versturen
    //Hier geef je enkel de id van het deleted item mee
    this.deleteRequest = function(url, handle, accessToken) {
        this.url = url;
        this.responseHandle = handle;
        this.accessToken = accessToken;
        this.method = 'DELETE';
        this.setupRequest();
    };


    // De lopende aanvraag/request annuleren
    this.abort = function() {
        if (this.request) {
            this.request.onreadystatechange = function() {};
            this.request.abort();
            this.request = null;
        }
    };
    
    
    
    



    // afhandelen van de errors
    this.errorHandle = function() {
        var errorWindow;
        // Toon een foutmelding in een popup window
        try {
            // errorWindow = window.open('', 'errorWin');
            errorWindow.document.body.innerHTML = this.responseText;
        }
        // Als pop-ups in de browser geblokkeerd zijn, zeg het aan de gebruiker
        catch (e) {
            // alert('Er is een fout opgetreden, maar de foutmelding kan niet ' +
            // ' getoond worden omdat je browser pop-ups blokkeert.\n' +
            // 'Je moet pop-ups toelaten voor die Web site als je de foutmeldingen wil zien.');
            var feedback = document.getElementById('feedback');
            if (!feedback) {
                feedback = document.createElement('div');
                document.body.appendChild(feedback);
            }
            var p = document.createElement('p');
            var textContent = document.createTextNode(this.responseText);
            p.appendChild(textContent);
            feedback.appendChild(p);
        }
    };

















}
