 //Door middel van de helper function wordt er feedback getoond bij het aanklikken van een folder/file. 
 //Dit resulteert in een text onderaan de pagina aangezien we hier een text parameter meegeven. 
 //Afhankelijk van het id van het element dat wordt opgehaald, zal er een p element met feedback meegegeven worden aan de html pagina.
 //Hier met de doorgestuurde text als text node
function showFeedback(text) {
    var feedback = document.getElementById('feedback');
    if (!feedback) {
        feedback = document.createElement('div');
        document.body.appendChild(feedback);
    }
    var p = document.createElement('p');
    var textContent = document.createTextNode(text);
    p.appendChild(textContent);
    feedback.appendChild(p);
}
