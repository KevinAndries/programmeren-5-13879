/**
google-oauth-api.js bevat:
1. de functies om zich aan- en af te melden via de Google people API
2. de functies om gebruikers-gegevens op te halen en te tonen 
**/

// CORS (Cross Origin Recourse Sharing) toegang tot google Driva API met ajax bibliotheek

// Methode die de API en de auth library zal laden
// auth2 module wordt geimporteerd om het mogelijk te maken in te loggen via de google account
// Op de gapi library wordt de load methode gebruikt (auth client laden)

function loadAuthClient() {
    gapi.load('client:auth2', initAuth);
}



// Methode om de OAuth te initialiseren
// Hier wordt de ClientID en de scope meegegeven
// Na verificatie wordt via de "Then" methode van de initmethode om na te gaan of de gebruiker is ingelogd
// Na verificatie wordt de handleInitialSignInStatus uitgevoerd die terug te vinden is in de HTML pagina
// We maken hier alsnog een aparte methode  binnen "google-oauth-api-js" van omdat we deze verificatie willen kunnen gebruiken voor al onze projecten

function initAuth() {
    //gapi.client.setApiKey(apiKey);
    //via gapi.auth2.init wordt de google loggin account geïnitialiseerd met onze clientID en scope (het oproepen ervan dus)
    gapi.auth2.init({
        client_id: clientId,
        scope: scopes.join(' '),
        immediate: true
    }).then(function() {
        //opvragen van de huidige status (in- of uitgelog)
        gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);
    }).then(handleInitialSignInStatus);
}




function updateSigninStatus(isSignedIn) {
    if (isSignedIn) {
        //Toon SignIn button niet
        signinButton.style.display = 'none';
        //Toon SignOut button wel
        signoutButton.style.display = 'block';
    }
    else {
        signinButton.style.display = 'block';
        signoutButton.style.display = 'none';
    }
}

//Via dit event wordt het google scherm getoond van waaruit er een profiel gekozen kan worden
function signIn(event) {
    gapi.auth2.getAuthInstance().signIn();
}

//Event voor user af te melden uit google account
function signOut(event) {
    gapi.auth2.getAuthInstance().signOut();
}




