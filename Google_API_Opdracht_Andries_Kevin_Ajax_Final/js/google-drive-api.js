/**
google-drive-api.js bevat:
1. File Manager: om te navigeren door de files, folders te maken/deleten/openen
2. Tekst Editor: om teksten toe te voegen, te bewerken en op te slaan
**/


// Functie om te gaan kijken of Myap reeds bestaat. 
// Zo ja, wordt deze geopend, zo nee zal er een Myap folder aangemaakt worden
function isMyapFolderPresent(responseText) {
    var response = JSON.parse(responseText);
    var item = document.getElementById('currentFolderId').value;
    var folderItem = response.files.find(function(item) {
            return item.name === 'myap';
        })
        // Hier wordt er gekeken naar de id van de root map om te controleren of de myap folder reeds bestaat.
    if (folderItem) {
        // openen en tonen van de folder + files die erin zitten
        getFilesInFolder(item.id, showFiles);
    }
    else {
        createFolder('myap');
    }
}


// Nieuwe folder aanmaken
// Hier wordt een folder aangemaakt in de root map van de Google Drive. 
// Dit wil zeggen dat die folder geen parentfolder heeft.
// Slechts één parameter aangezien folder in de root komt te staan
function createFolder(name) {
    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    // instantie van het Ajax Object maken
    var ajax = new Ajax();
    //In de body een JasonString meegeven met daarin de naam en het mimetype --> zie data
    var data = {
            name: name,
            // mimetype geeft aan dat we een bestand van het type folder willen maken.
            mimeType: "application/vnd.google-apps.folder"
        }
        // aanmaken van een folder = POST naar de server
    ajax.postRequest('https://www.googleapis.com/drive/v3/files',
        //In de body een JasonString meegeven met daarin de naam en het mimetype --> zie data
        JSON.stringify(data),
        function(responseText) {
            alert(responseText);
        },
        //opgeven dat het antwoord van de server text is
        //en header toevoegen waarin we aan de server meedelen dat de gegevens in de body van het gegevenstype JSON zijn
        'text',
        'application/json',
        accessToken);
}




// Voorbereidende werk op de CreateFolderInParent. Via de getFilesInFolder hebben een manier gevonden om te weten welke folder de gebruiker geselecteerd heeft adhv id en naam --> input elementen
// Prepare the creation of a new folder
// Validate the foldername
// Check if new foldername does already exist
function prepareCreateFolder() {
    var folderName = document.getElementById('folderName').value;
    // haal de Id van de parent folder op
    var parentFolderId = document.getElementById('currentFolderId').value;
    var parentFolderName = document.getElementById('currentFolderName').value;

    // Hier voeren we de controle uit of er effectief iets in de folder staat. 
    // Dit doen we met het .length attribuut
    if (folderName.length > 0) {
        // Reguliere expressie om te zien of er geen andere karakters in staan
        var regExpresion = new RegExp("[^a-zA-Z0-9_. ]+");
        if (regExpresion.test(folderName)) {
            alert('Ongeldige foldernaam: ' + folderName)
        }
        else {
            // haal alle bestanden van de google drive op en verifiëer
            // Zitten we in de root, gaan we via de getfiles alle bestanden ophalen
            // voor elk bestand gaan we dan de lus doesFolderExist oproepen
            // als de folder al bestaat, daarvoor geven we de callback
            // functie doesFolderExist mee
            if (parentFolderId == 'root') {
                getFiles(function(responseText) {
                    doesFolderExist(responseText,
                        folderName, parentFolderId);
                });
            }
            else {
                // als de gebruiker een map geselecteerd heeft halen
                // we alleen de bestanden in de geselecteerde map op
                // dit doen we door de doesFolderExist methode te gebruiken
                getFilesInFolder(parentFolderId,
                    parentFolderName,
                    function(responseText) {
                        doesFolderExist(responseText,
                            folderName, parentFolderId);
                    });
            }
        }
    }
    else {
        alert('Typ eerst een naam voor de folder in.');
    }
}


// Hier gaan we na of de folder al bestaat of niet.
// Folder bestaat: boodschap dat deze bestaat. 
// Indien in de root --> createFolder, ander CreateFolderInParent
// Folder bestaat niet: 

// Drie parameters:
// responseText: JSON array met de naam van de bestanden in de root of in de geselecteerde folder;
// folderName: de naam van de te maken folder;
// parentFolderId: de id van de folder waarin de map gemaakt moet worden;

var doesFolderExist = function(responseText, folderName, parentFolderId) {
    var response = JSON.parse(responseText);
    // check is the folder already exists
    // names not case sensitive
    if (response.files.some(function(item) {
            return item.name.toLowerCase() === folderName.toLowerCase()
        })) {
        alert('Folder met de naam ' + folderName + ' bestaat al!')
    }
    else {
        if (folderName == 'root') {
            createFolder(folderName);
        }
        else {
            createFolderInParent(folderName, parentFolderId);
        }
    }
}






// Zie CreateFolder
// Doet hetzefde, maar heeft als extra parameter de id van de folder waarin de nieuwe folder gemaakt moet worden. 
function createFolderInParent(name, parentId) {
    var data = {
        'name': name,
        'mimeType': "application/vnd.google-apps.folder",
        // parent id waarin we de folder willen creëeren.
        // extra veld in body waarin we deze id toevoegen. 
        'parents': [parentId]
    };

    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    // or this: gapi.auth.getToken().access_token;
    var ajax = new Ajax();
    ajax.postRequest('https://www.googleapis.com/drive/v3/files',
        JSON.stringify(data),
        function(responseText) {
            alert(responseText);
        },
        'text',
        'application/json',
        accessToken);
}











// Bestanden ophalen
// We geven hier de callbackFunction methode mee als parameter mee aangezien we niet op voorhand weten wat we met de bestanden gaan doen
// Geven we geen methode mee, worden de files getoond
// Deze gaan we ophalen van de Google Drive
var getFiles = function(callbackFunction = showFiles) {
    //Accesstoken ophalen die wordt opgevraagd via de getAuthInstance() method van Google
    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    // Instantie van de Ajax Klasse aanmaken (opbouwen van de ajax call)
    // this: gapi.auth.getToken().access_token;
    var ajax = new Ajax();
    // Google CORS
    var url = 'https://www.googleapis.com/drive/v3/files';
    // Query waarbij we de deleted items niet willen zien
    url += '?q=';
    url += 'not+trashed';
    // fields (opgeven welke attributen we willen bekomen)
    url += '&';
    url += 'fields=files(iconLink%2Cname%2CmimeType%2Cid%2Cparents)'; // %2C is html code voor een komma (,)
    // Hier roepen we de getRequest methode van de Ajax Klasse op.
    // Hier geven de url, callbackFunction en access token mee. De callback functie krijgt zo return values mee die nadien aan de callbackfunctie showFiles kunnen meegegeven worden
    ajax.getRequest(url, callbackFunction, 'text', accessToken);
    // onthoud de id en de naam van de geselecteerde folder
    document.getElementById('currentFolderId').value = 'root';
    document.getElementById('currentFolderName').value = 'root';
}



// Hier maken we een opsomming van alle items. 
// Dit in de vorm van een lijst met bullets onder elkaar <li><ul>
// Wanneer het effectief een folder is ipv een item kunnen we doorklikken om verder in de folderstructuur te gaan en de inhoud te bekijken
// responseText komt binnen via de ajax.getRequest() functie showFiles en zal worden aangeroepen door even ajax.getRequest
var showFiles = function(responseText) {
    //Referentie naar HTML ophalen met id = #explorer #list
    var ul = document.querySelector("#explorer #list");
    //Lijst clearen
    ul.innerHTML = '';
    var li = document.createElement("li");
    //Knop toevoegen om naar de root te gaan
    // li.innerHTML = '<button type="button" onclick="getFiles();">Terug naar root</button><br><br><br>';
    // // listitem toevoegen aan de ul
    // ul.appendChild(li);


    //Antwoord server is in Jasonformaat, dus dient omgezet te worden naar array met JSON.parse
    var response = JSON.parse(responseText);
    for (var i = 0; i < response.files.length; i++) {
        var item = response.files[i];
        //li element aanmaken voor elk element in de array
        //Indien het een folder is --> knop maken om door te kunnen klikken naar inhoud folder
        //id en name worden meegegeven
        var li = document.createElement("li");
        var html = "<img src='" + item.iconLink + "'> ";
                var renameFileBtn = '<button type="button" onclick="renameFile(' + '\'' +
            item.id + '\', \'' + item.mimeType + '\');">rename</button>'  ;
        // nakijken of item geen folder is, zo ja, button aanmaken
        if (item.mimeType == 'application/vnd.google-apps.folder') {
            html += '<button type="button" onclick="getFilesInFolder(\'' +
                item.id + '\', \'' + item.name + '\');">' + item.name + '</button>';
        }
        // else is het een file
        else {
            html += item.name;
            // only html
            if (item.name.indexOf('.html') > -1) {
                html += ' <button type="button" onclick="downloadText(' + '\'' +
                    item.id + '\', \'' + item.name + '\');">open</button>'
            }
        }
        //Bij elk elemenent een delete knop plaatsen
        //onClick = de naam van de functie "deteleFiles". Heeft als parameters id en name
        html += ' <button type="button" onclick="deleteFile(' + '\'' +
            item.id + '\', \'' + item.name + '\');">delete</button>';
       
        li.innerHTML = html;
        ul.appendChild(li);
    }
    showFeedback(responseText);
}








// Bestanden ophalen
// We geven hier de callbackFunction methode mee als parameter mee aangezien we niet op voorhand weten wat we met de bestanden gaan doen
// Geven we geen methode mee, worden de files getoond
// Deze gaan we ophalen van de Google Drive

// Methode heeft twee parameters: 
// id van de folder waarvan we de bestanden willen ophalen 
// naam van de callback functie die moet worden uitgevoerd als de server geantwoord heeft. 
function getFilesInFolder(id, name, callbackFunction = showFiles) {
    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    // or this: gapi.auth.getToken().access_token;
    var ajax = new Ajax();
    // google CORS
    var url = 'https://www.googleapis.com/drive/v3/files';
    // Geen verwijderde items
    url += '?q=';
    url += 'not+trashed';
    url += '+and+';
    url += '\'' + id + '\'+in+parents';
    // fields
    url += '&';
    url += 'fields=files(iconLink%2Cname%2CmimeType%2Cid%2Cparents)';
    //alert(url);
    ajax.getRequest(url, callbackFunction, 'text', accessToken);
    // onthoud de id en de naam van de geselecteerde folder
    document.getElementById('currentFolderId').value = id;
    document.getElementById('currentFolderName').value = name;
}







// Methode om bestand  of folder te deleten op de google Drive
// REST API krijgt woord DELETE mee vanuit deleteRequest

// Twee parameters
// id: de id van de te deleten folder of bestand;
// name: de naam van de te deleten folder of bestand; de naam hebben we nodig in de feedback aan de gebruiker;
function deleteFile(id, name) {
    var ajax = new Ajax();
    // Google CORS
    var url = 'https://www.googleapis.com/drive/v3/files';
    // id
    url += '/' + id;
    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    // or this: gapi.auth.getToken().access_token;
    ajax.deleteRequest(url,
        function(responseText) {
            alert(name + ' is gedeleted!');
        },
        accessToken);
}


// Voorbereidend werk voor het uploaden van een tekst vanuit de textArea naar de GoogleDrive
function prepareUploadText() {

    // tekst ophalen uit editor
    var text = document.getElementById('editor').value;
    if (text.length > 0) {
        // naam van het bestand ophalen
        var name = text.match(/<h1>(.*?)<\/h1>/g);
        if (!name) {
            alert('Je moet een h1 element toevoegen waarin de naam van het bestand staat!');
        }
        else {
            // we need the first match
            var fileName = name[0];
            // remove tags, dat kan waarschijnlijk eleganter...
            // maar ik heb hier al genoeg over moeten nadenken
            fileName = fileName.replace('<h1>', '');
            fileName = fileName.replace('</h1>', '');
            fileName = fileName.replace(/[^a-zA-Z0-9_. ]+/, '');
            // haal de Id van de parent folder op
            var parentFolderId = document.getElementById('currentFolderId').value;
            // oproepen uploadText methode die de tekst als bestand met opgegeven naam naar google drive upload
            uploadText(fileName, text, parentFolderId);
        }
    }
    else {
        alert('Editor is leeg, kan geen leeg bestand uploaden...');
    }
}



// Parameters
// metadata: de naam, mime type van het bestand;
// tekst: de inhoud van het bestand, de tekst die in de editor is ingetypt:
function uploadText(name, text, parentFolderId) {

    // body request opdelen in twee delen.
    // metaData is application/json type
    var metaData = {
        'name': name + '.html',
        'parents': [parentFolderId]
    };
    // inhoud is van het aapplication/text type
    var data = '--next_section\r\n' +
        // Content-type instellen op multipart/related --> server weet zo in welke indeling de body van de request is opgesteld
        'Content-Type: application/json; charset=UTF-8\r\n\r\n' +
        JSON.stringify(metaData) +
        '\r\n\r\n--next_section\r\n' +
        'Content-Type: application/text\r\n\r\n' +
        text +
        '\r\n--next_section--';

    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    // or this: gapi.auth.getToken().access_token;
    var ajax = new Ajax();
    ajax.postRequest('https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart',
        data,
        function(responseText) {
            alert(responseText);
        },
        'text',
        // Content-type instellen op multipart/related --> server weet zo in welke indeling de body van de request is opgesteld
        // door parameter multipart in te vullen weet Google API dat de body metadata als inhoud bevat
        'multipart/related; boundary=next_section',
        accessToken);
}

// Via eeen GET request geven we de id voort het te openen bestand op de url mee.
// In de callback stoppen we de geretourneerde tekst in het HTML element met id='editor'
function downloadText(id, name) {
    var ajax = new Ajax();
    // Google CORS
    var url = 'https://www.googleapis.com/drive/v3/files';
    // id
    url += '/' + id;
    // download
    url += '?alt=media';
    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    // or this: gapi.auth.getToken().access_token;
    ajax.getRequest(url,
        function(responseText) {
            document.getElementById('editor').innerHTML = responseText;
        },
        'text',
        accessToken);
}




// Functie voor het aanmaken van een back
function makeBackUp(callbackFunction = autoBackup) {
    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    // or this: gapi.auth.getToken().access_token;
    var ajax = new Ajax();
    // Google CORS
    var url = 'https://www.googleapis.com/drive/v3/files';
    // query
    url += '?q=';
    url += 'not+trashed';
    // fields
    url += '&';
    url += 'fields=files(iconLink%2Cname%2CmimeType%2Cid%2Cparents)'; // %2C is html code for a comma (,)
    ajax.getRequest(url, callbackFunction, 'text', accessToken);
}


function autoBackup(responseText) {

    var text = document.getElementById('editor').value;
    if (text.length > 0) {
        // get the file name
        // Eerst gedacht met parser to doen, maar dit is te streng
        // https://developer.mozilla.org/en-US/docs/Web/API/DOMParser
        // var parser = new DOMParser();
        // var doc = parser.parseFromString(text, "application/xml");
        // if (doc.getElementsByTagName('h1').length == 0) {
        var name = text.match(/<h1>(.*?)<\/h1>/g);
        if (!name) {
            alert('Je moet een h1 element toevoegen waarin de naam van het bestand staat!');
        }
        else {
            // we need the first match
            var fileName = name[0];
            // remove tags, dat kan waarschijnlijk eleganter...
            // maar ik heb hier al genoeg over moeten nadenken
            fileName = fileName.replace('<h1>', '');
            fileName = fileName.replace('</h1>', '');
            fileName = fileName.replace(/[^a-zA-Z0-9_. ]+/, '');
            fileName += '.html';
        }
    }
    else {
        // Opgelet, editor mag niet leeg zijn
        alert('Editor is leeg, kan geen leeg bestand uploaden...');
    }


    var response = JSON.parse(responseText);
    var teller = 0;
    for (var i = 0; i < response.files.length; i++) {
        var item = response.files[i];

        if (item.name == fileName) {
            teller++
        }
    }
    // Indien cijfer 1 is, wil dit zeggen dat dat de filename hetzelfde is
    // enkel als er een aangepast bestand is, zal er een back up opgeslagen worden
    if (teller == 0) {
        prepareUploadText();
    }
}
/**
google-drive-api.js bevat:
1. File Manager: om te navigeren door de files, folders te maken/deleten/openen
2. Tekst Editor: om teksten toe te voegen, te bewerken en op te slaan
**/


// Functie om te gaan kijken of Myap reeds bestaat. 
// Zo ja, wordt deze geopend, zo nee zal er een Myap folder aangemaakt worden
function isMyapFolderPresent(responseText) {
    var response = JSON.parse(responseText);
    var item = document.getElementById('currentFolderId').value;
    var folderItem = response.files.find(function(item) {
            return item.name === 'myap';
        })
        // Hier wordt er gekeken naar de id van de root map om te controleren of de myap folder reeds bestaat.
    if (folderItem) {
        // openen en tonen van de folder + files die erin zitten
        getFilesInFolder(item.id, showFiles);
    }
    else {
        createFolder('myap');
    }
}


// Nieuwe folder aanmaken
// Hier wordt een folder aangemaakt in de root map van de Google Drive. 
// Dit wil zeggen dat die folder geen parentfolder heeft.
// Slechts één parameter aangezien folder in de root komt te staan
function createFolder(name) {
    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    // instantie van het Ajax Object maken
    var ajax = new Ajax();
    //In de body een JasonString meegeven met daarin de naam en het mimetype --> zie data
    var data = {
            name: name,
            // mimetype geeft aan dat we een bestand van het type folder willen maken.
            mimeType: "application/vnd.google-apps.folder"
        }
        // aanmaken van een folder = POST naar de server
    ajax.postRequest('https://www.googleapis.com/drive/v3/files',
        //In de body een JasonString meegeven met daarin de naam en het mimetype --> zie data
        JSON.stringify(data),
        function(responseText) {
            alert(responseText);
        },
        //opgeven dat het antwoord van de server text is
        //en header toevoegen waarin we aan de server meedelen dat de gegevens in de body van het gegevenstype JSON zijn
        'text',
        'application/json',
        accessToken);
}




// Voorbereidende werk op de CreateFolderInParent. Via de getFilesInFolder hebben een manier gevonden om te weten welke folder de gebruiker geselecteerd heeft adhv id en naam --> input elementen
// Prepare the creation of a new folder
// Validate the foldername
// Check if new foldername does already exist
function prepareCreateFolder() {
    var folderName = document.getElementById('folderName').value;
    // haal de Id van de parent folder op
    var parentFolderId = document.getElementById('currentFolderId').value;
    var parentFolderName = document.getElementById('currentFolderName').value;

    // Hier voeren we de controle uit of er effectief iets in de folder staat. 
    // Dit doen we met het .length attribuut
    if (folderName.length > 0) {
        // Reguliere expressie om te zien of er geen andere karakters in staan
        var regExpresion = new RegExp("[^a-zA-Z0-9_. ]+");
        if (regExpresion.test(folderName)) {
            alert('Ongeldige foldernaam: ' + folderName)
        }
        else {
            // haal alle bestanden van de google drive op en verifiëer
            // Zitten we in de root, gaan we via de getfiles alle bestanden ophalen
            // voor elk bestand gaan we dan de lus doesFolderExist oproepen
            // als de folder al bestaat, daarvoor geven we de callback
            // functie doesFolderExist mee
            if (parentFolderId == 'root') {
                getFiles(function(responseText) {
                    doesFolderExist(responseText,
                        folderName, parentFolderId);
                });
            }
            else {
                // als de gebruiker een map geselecteerd heeft halen
                // we alleen de bestanden in de geselecteerde map op
                // dit doen we door de doesFolderExist methode te gebruiken
                getFilesInFolder(parentFolderId,
                    parentFolderName,
                    function(responseText) {
                        doesFolderExist(responseText,
                            folderName, parentFolderId);
                    });
            }
        }
    }
    else {
        alert('Typ eerst een naam voor de folder in.');
    }
}


// Hier gaan we na of de folder al bestaat of niet.
// Folder bestaat: boodschap dat deze bestaat. 
// Indien in de root --> createFolder, ander CreateFolderInParent
// Folder bestaat niet: 

// Drie parameters:
// responseText: JSON array met de naam van de bestanden in de root of in de geselecteerde folder;
// folderName: de naam van de te maken folder;
// parentFolderId: de id van de folder waarin de map gemaakt moet worden;

var doesFolderExist = function(responseText, folderName, parentFolderId) {
    var response = JSON.parse(responseText);
    // check is the folder already exists
    // names not case sensitive
    if (response.files.some(function(item) {
            return item.name.toLowerCase() === folderName.toLowerCase()
        })) {
        alert('Folder met de naam ' + folderName + ' bestaat al!')
    }
    else {
        if (folderName == 'root') {
            createFolder(folderName);
        }
        else {
            createFolderInParent(folderName, parentFolderId);
        }
    }
}






// Zie CreateFolder
// Doet hetzefde, maar heeft als extra parameter de id van de folder waarin de nieuwe folder gemaakt moet worden. 
function createFolderInParent(name, parentId) {
    var data = {
        'name': name,
        'mimeType': "application/vnd.google-apps.folder",
        // parent id waarin we de folder willen creëeren.
        // extra veld in body waarin we deze id toevoegen. 
        'parents': [parentId]
    };

    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    // or this: gapi.auth.getToken().access_token;
    var ajax = new Ajax();
    ajax.postRequest('https://www.googleapis.com/drive/v3/files',
        JSON.stringify(data),
        function(responseText) {
            alert(responseText);
        },
        'text',
        'application/json',
        accessToken);
}











// Bestanden ophalen
// We geven hier de callbackFunction methode mee als parameter mee aangezien we niet op voorhand weten wat we met de bestanden gaan doen
// Geven we geen methode mee, worden de files getoond
// Deze gaan we ophalen van de Google Drive
var getFiles = function(callbackFunction = showFiles) {
    //Accesstoken ophalen die wordt opgevraagd via de getAuthInstance() method van Google
    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    // Instantie van de Ajax Klasse aanmaken (opbouwen van de ajax call)
    // this: gapi.auth.getToken().access_token;
    var ajax = new Ajax();
    // Google CORS
    var url = 'https://www.googleapis.com/drive/v3/files';
    // Query waarbij we de deleted items niet willen zien
    url += '?q=';
    url += 'not+trashed';
    // fields (opgeven welke attributen we willen bekomen)
    url += '&';
    url += 'fields=files(iconLink%2Cname%2CmimeType%2Cid%2Cparents)'; // %2C is html code voor een komma (,)
    // Hier roepen we de getRequest methode van de Ajax Klasse op.
    // Hier geven de url, callbackFunction en access token mee. De callback functie krijgt zo return values mee die nadien aan de callbackfunctie showFiles kunnen meegegeven worden
    ajax.getRequest(url, callbackFunction, 'text', accessToken);
    // onthoud de id en de naam van de geselecteerde folder
    document.getElementById('currentFolderId').value = 'root';
    document.getElementById('currentFolderName').value = 'root';
}



// Hier maken we een opsomming van alle items. 
// Dit in de vorm van een lijst met bullets onder elkaar <li><ul>
// Wanneer het effectief een folder is ipv een item kunnen we doorklikken om verder in de folderstructuur te gaan en de inhoud te bekijken
// responseText komt binnen via de ajax.getRequest() functie showFiles en zal worden aangeroepen door even ajax.getRequest
var showFiles = function(responseText) {
    //Referentie naar HTML ophalen met id = #explorer #list
    var ul = document.querySelector("#explorer #list");
    //Lijst clearen
    ul.innerHTML = '';
    var li = document.createElement("li");
    //Knop toevoegen om naar de root te gaan
    // li.innerHTML = '<button type="button" onclick="getFiles();">Terug naar root</button><br><br><br>';
    // // listitem toevoegen aan de ul
    // ul.appendChild(li);


    //Antwoord server is in Jasonformaat, dus dient omgezet te worden naar array met JSON.parse
    var response = JSON.parse(responseText);
    for (var i = 0; i < response.files.length; i++) {
        var item = response.files[i];
        //li element aanmaken voor elk element in de array
        //Indien het een folder is --> knop maken om door te kunnen klikken naar inhoud folder
        //id en name worden meegegeven
        var li = document.createElement("li");
        var html = "<img src='" + item.iconLink + "'> ";
                var renameFileBtn = '<button type="button" onclick="renameFile(' + '\'' +
            item.id + '\', \'' + item.mimeType + '\');">rename</button>'  ;
        // nakijken of item geen folder is, zo ja, button aanmaken
        if (item.mimeType == 'application/vnd.google-apps.folder') {
            html += '<button type="button" onclick="getFilesInFolder(\'' +
                item.id + '\', \'' + item.name + '\');">' + item.name + '</button>';
        }
        // else is het een file
        else {
            html += item.name;
            // only html
            if (item.name.indexOf('.html') > -1) {
                html += ' <button type="button" onclick="downloadText(' + '\'' +
                    item.id + '\', \'' + item.name + '\');">open</button>'
            }
        }
        //Bij elk elemenent een delete knop plaatsen
        //onClick = de naam van de functie "deteleFiles". Heeft als parameters id en name
        html += ' <button type="button" onclick="deleteFile(' + '\'' +
            item.id + '\', \'' + item.name + '\');">delete</button>';
       
        li.innerHTML = html;
        ul.appendChild(li);
    }
    showFeedback(responseText);
}








// Bestanden ophalen
// We geven hier de callbackFunction methode mee als parameter mee aangezien we niet op voorhand weten wat we met de bestanden gaan doen
// Geven we geen methode mee, worden de files getoond
// Deze gaan we ophalen van de Google Drive

// Methode heeft twee parameters: 
// id van de folder waarvan we de bestanden willen ophalen 
// naam van de callback functie die moet worden uitgevoerd als de server geantwoord heeft. 
function getFilesInFolder(id, name, callbackFunction = showFiles) {
    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    // or this: gapi.auth.getToken().access_token;
    var ajax = new Ajax();
    // google CORS
    var url = 'https://www.googleapis.com/drive/v3/files';
    // Geen verwijderde items
    url += '?q=';
    url += 'not+trashed';
    url += '+and+';
    url += '\'' + id + '\'+in+parents';
    // fields
    url += '&';
    url += 'fields=files(iconLink%2Cname%2CmimeType%2Cid%2Cparents)';
    //alert(url);
    ajax.getRequest(url, callbackFunction, 'text', accessToken);
    // onthoud de id en de naam van de geselecteerde folder
    document.getElementById('currentFolderId').value = id;
    document.getElementById('currentFolderName').value = name;
}







// Methode om bestand  of folder te deleten op de google Drive
// REST API krijgt woord DELETE mee vanuit deleteRequest

// Twee parameters
// id: de id van de te deleten folder of bestand;
// name: de naam van de te deleten folder of bestand; de naam hebben we nodig in de feedback aan de gebruiker;
function deleteFile(id, name) {
    var ajax = new Ajax();
    // Google CORS
    var url = 'https://www.googleapis.com/drive/v3/files';
    // id
    url += '/' + id;
    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    // or this: gapi.auth.getToken().access_token;
    ajax.deleteRequest(url,
        function(responseText) {
            alert(name + ' is gedeleted!');
        },
        accessToken);
}


// Voorbereidend werk voor het uploaden van een tekst vanuit de textArea naar de GoogleDrive
function prepareUploadText() {

    // tekst ophalen uit editor
    var text = document.getElementById('editor').value;
    if (text.length > 0) {
        // naam van het bestand ophalen
        var name = text.match(/<h1>(.*?)<\/h1>/g);
        if (!name) {
            alert('Je moet een h1 element toevoegen waarin de naam van het bestand staat!');
        }
        else {
            // we need the first match
            var fileName = name[0];
            // remove tags, dat kan waarschijnlijk eleganter...
            // maar ik heb hier al genoeg over moeten nadenken
            fileName = fileName.replace('<h1>', '');
            fileName = fileName.replace('</h1>', '');
            fileName = fileName.replace(/[^a-zA-Z0-9_. ]+/, '');
            // haal de Id van de parent folder op
            var parentFolderId = document.getElementById('currentFolderId').value;
            // oproepen uploadText methode die de tekst als bestand met opgegeven naam naar google drive upload
            uploadText(fileName, text, parentFolderId);
        }
    }
    else {
        alert('Editor is leeg, kan geen leeg bestand uploaden...');
    }
}



// Parameters
// metadata: de naam, mime type van het bestand;
// tekst: de inhoud van het bestand, de tekst die in de editor is ingetypt:
function uploadText(name, text, parentFolderId) {

    // body request opdelen in twee delen.
    // metaData is application/json type
    var metaData = {
        'name': name + '.html',
        'parents': [parentFolderId]
    };
    // inhoud is van het aapplication/text type
    var data = '--next_section\r\n' +
        // Content-type instellen op multipart/related --> server weet zo in welke indeling de body van de request is opgesteld
        'Content-Type: application/json; charset=UTF-8\r\n\r\n' +
        JSON.stringify(metaData) +
        '\r\n\r\n--next_section\r\n' +
        'Content-Type: application/text\r\n\r\n' +
        text +
        '\r\n--next_section--';

    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    // or this: gapi.auth.getToken().access_token;
    var ajax = new Ajax();
    ajax.postRequest('https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart',
        data,
        function(responseText) {
            alert(responseText);
        },
        'text',
        // Content-type instellen op multipart/related --> server weet zo in welke indeling de body van de request is opgesteld
        // door parameter multipart in te vullen weet Google API dat de body metadata als inhoud bevat
        'multipart/related; boundary=next_section',
        accessToken);
}

// Via eeen GET request geven we de id voort het te openen bestand op de url mee.
// In de callback stoppen we de geretourneerde tekst in het HTML element met id='editor'
function downloadText(id, name) {
    var ajax = new Ajax();
    // Google CORS
    var url = 'https://www.googleapis.com/drive/v3/files';
    // id
    url += '/' + id;
    // download
    url += '?alt=media';
    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    // or this: gapi.auth.getToken().access_token;
    ajax.getRequest(url,
        function(responseText) {
            document.getElementById('editor').innerHTML = responseText;
        },
        'text',
        accessToken);
}




// Functie voor het aanmaken van een back
function makeBackUp(callbackFunction = autoBackup) {
    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    // or this: gapi.auth.getToken().access_token;
    var ajax = new Ajax();
    // Google CORS
    var url = 'https://www.googleapis.com/drive/v3/files';
    // query
    url += '?q=';
    url += 'not+trashed';
    // fields
    url += '&';
    url += 'fields=files(iconLink%2Cname%2CmimeType%2Cid%2Cparents)'; // %2C is html code for a comma (,)
    ajax.getRequest(url, callbackFunction, 'text', accessToken);
}


function autoBackup(responseText) {

    var text = document.getElementById('editor').value;
    if (text.length > 0) {
        // get the file name
        // Eerst gedacht met parser to doen, maar dit is te streng
        // https://developer.mozilla.org/en-US/docs/Web/API/DOMParser
        // var parser = new DOMParser();
        // var doc = parser.parseFromString(text, "application/xml");
        // if (doc.getElementsByTagName('h1').length == 0) {
        var name = text.match(/<h1>(.*?)<\/h1>/g);
        if (!name) {
            alert('Je moet een h1 element toevoegen waarin de naam van het bestand staat!');
        }
        else {
            // we need the first match
            var fileName = name[0];
            // remove tags, dat kan waarschijnlijk eleganter...
            // maar ik heb hier al genoeg over moeten nadenken
            fileName = fileName.replace('<h1>', '');
            fileName = fileName.replace('</h1>', '');
            fileName = fileName.replace(/[^a-zA-Z0-9_. ]+/, '');
            fileName += '.html';
        }
    }
    else {
        // Opgelet, editor mag niet leeg zijn
        alert('Editor is leeg, kan geen leeg bestand uploaden...');
    }


    var response = JSON.parse(responseText);
    var teller = 0;
    for (var i = 0; i < response.files.length; i++) {
        var item = response.files[i];

        if (item.name == fileName) {
            teller++
        }
    }
    // Indien cijfer 1 is, wil dit zeggen dat dat de filename hetzelfde is
    // enkel als er een aangepast bestand is, zal er een back up opgeslagen worden
    if (teller == 0) {
        prepareUploadText();
    }
}
